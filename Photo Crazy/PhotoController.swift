//
//  ViewController.swift
//  Photo Crazy
//
//  Created by Admin on 16/04/17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Photos
import Social

class PhotoController: UICollectionViewController {
    
    
    @IBOutlet weak var imageView: UIImageView!
    var editor : AdobeUXImageEditorViewController?
    let picker = UIImagePickerController()
    var imageArray = [UIImage]()

  

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "PHOTO CRAZY"
        
          self.navigationController?.navigationBar.barTintColor = .black
        if AdobeUXAuthManager.shared().isAuthenticated {
            print(AdobeUXAuthManager.shared().userProfile)
        
        }


       collectionView?.delegate = self
        collectionView?.dataSource = self
        
        
//        feacthPhoto()
    }
   
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        

        PHPhotoLibrary.requestAuthorization { (statusAuthorization) in
            switch statusAuthorization {
            case .authorized:
                self.feacthPhoto()
                
            default:
                print("Não estou autorizado")
            }
        }
        
     
    }
   
 
    func feacthPhoto(){
        
        
       let imgManager = PHImageManager.default()
        
        
        
        let allPhotosOptions = PHImageRequestOptions()
        allPhotosOptions.isSynchronous  = true
        allPhotosOptions.deliveryMode = .highQualityFormat
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
         let fetchRequest : PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            
            if fetchRequest.count > 0 {
                for i in 0 ..< fetchRequest.count{
                    imgManager.requestImage(for: fetchRequest.object(at: i) , targetSize: CGSize(width:200, height:200), contentMode: .aspectFill, options: allPhotosOptions, resultHandler: {
                        image, error in
                        self.imageArray.append(image!)
                        
                    })
                }
            }
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }
        
    }
    
    func salveFoto(image: UIImage){
       
        guard let imageData  = image.jpegData(compressionQuality: 0.6) else{ return }
        guard let compressedJPGImage = UIImage(data: imageData) else { return }
        UIImageWriteToSavedPhotosAlbum(compressedJPGImage, nil, nil, nil)
        self.imageArray.insert(image, at: 0)
        self.collectionView?.reloadData()
    }
    
    
    func showEditor(image: UIImage){
        self.editor = AdobeUXImageEditorViewController(image: image)
        self.editor?.delegate = self
        picker.allowsEditing = true
        self.present(self.editor!, animated: true, completion: nil)
        
        _ =   editor?.enqueueHighResolutionRender(with: image) { (imagem, error) in
            if  error != nil {
                print("error " + (error?.localizedDescription)!)
                return
            }
             guard let  img  = imagem else { return }
            self.salveFoto(image: img)
            
        }
    }
    
}
extension PhotoController : AdobeUXImageEditorViewControllerDelegate,  UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,
UINavigationControllerDelegate{
    func photoEditorCanceled(_ editor: AdobeUXImageEditorViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    func photoEditor(_ editor: AdobeUXImageEditorViewController, finishedWith image: UIImage?) {
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
   

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count 
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = imageArray[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return  UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      self.showEditor(image: imageArray[indexPath.row])
    }
    
}
